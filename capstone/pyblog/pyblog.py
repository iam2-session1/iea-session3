#! /usr/bin/env python3
import argparse
import base64
import requests
import os
import sys


def get_auth_token(wordpress_username, wordpress_password):
    """ This function takes 2 args (wordpress_username and wordpress_password)
    and will return a base64 encoded basic auth token to call wordpress api """
    auth_string = f"{wordpress_username}:{wordpress_password}"
    # Base64 requires bytes
    auth_bytes = auth_string.encode("utf8")
    auth_token = base64.b64encode(auth_bytes)
    auth = auth_token.decode("utf8")
    header = {"Authorization": f"Basic {auth}"}
    return header


def read(wordpress_username, wordpress_password, wordpress_url, show_number_of_posts=1):
    """ This will read from the wordpress blog and pull down the latest post.
    The function takes 3 argumnets wordpress_username, wordpress_password, wordpress_url. """
    header = get_auth_token(wordpress_username, wordpress_password)
    url = f"http://{wordpress_url}/wp-json/wp/v2/posts?per_page={show_number_of_posts}"

    try:
        response = requests.get(url, headers=header, timeout=10)
        response.raise_for_status()
    except requests.ConnectionError:
        print(
            "The request timed out. Please double check the information you are passing and try again"
        )
        return None
    except requests.HTTPError:
        print(
            "Oops. Something went wrong. Please double check credentials and try again."
        )
        return None

    blog = response.json()
    title = blog[0]["title"]["rendered"]
    body = blog[0]["content"]["rendered"]
    post = {"title": title, "body": body}

    return post


def upload(
    wordpress_username, wordpress_password, wordpress_url, filename, blog_post_title
):
    """ This function will upload a file to wordpress using the api.
        Parameters:
        string (wordpress_username), string (wordpress_password),
        string (wordpress_url),string (filename), string (blog_post_title) """

    header = get_auth_token(wordpress_username, wordpress_password)

    if filename and filename != "-":
        if not os.path.isfile(filename):
            raise FileNotFoundError(f"Please double check that {filename} exists")
        with open(filename) as f:
            content = f.readlines()
            content = "".join(content)
    elif filename == "-":
        content = sys.stdin.readlines()
        content = "".join(content)

    if len(content) == 0:
        print("Cannot pass an empty file")
        return None

    url = f"http://{wordpress_url}/wp-json/wp/v2/posts?title={blog_post_title}&status=publish&content={content}"

    try:
        response = requests.post(url, headers=header, timeout=10)
        response.raise_for_status()
        status = response.status_code
    except requests.ConnectionError:
        print(
            "The request timed out. Please double check the information you are using and try again. "
        )
        return None
    except requests.HTTPError:
        print(
            "Oops. Something went wrong. Please double check credentials and try again."
        )
        return None

    return status


def args_parser(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(prog="pyblog.py")

    parser.add_argument(
        "action",
        choices=["read", "upload"],
        help="specify whether to read or upload a post to the blog",
    )
    parser.add_argument(
        "-f",
        "--file",
        type=str,
        help="specify the path of the file to upload to wordpress",
    )
    parser.add_argument("-t", "--title", help="Title of the blog post")

    args = parser.parse_args(argv)

    return args


def main(args=None):
    args = args_parser(args)

    if args.action == "upload":
        status = upload(
            os.environ["USERNAME"],
            os.environ["PASSWORD"],
            os.environ["ENVIRONMENT"],
            args.file,
            args.title,
        )
        print(status)
        return status
    elif args.action == "read":
        read_post = read(
            os.environ["USERNAME"], os.environ["PASSWORD"], os.environ["ENVIRONMENT"]
        )
        print(read_post)
        return read_post


if __name__ == "__main__":
    main(sys.argv[1:])
