#!/usr/bin/env python3
import requests
import sys

def currentWeather():
    try:
        api_call = requests.get("https://pm9epzfrgl.execute-api.us-east-1.amazonaws.com/default/kenchen_weather")
        results = api_call.json()
        print(results)
        api_call.raise_for_status()   
    except requests.exceptions.HTTPError as e:
        print(e)
        return None

def timedWeather(time):
    try:
        api_call = requests.get(f"https://pm9epzfrgl.execute-api.us-east-1.amazonaws.com/default/kenchen_current_weather?time={time}")
        results = api_call.json()
        print(results)
        api_call.raise_for_status()   
    except requests.exceptions.HTTPError as e:
        print(e)
        return None

if __name__=="__main__":
    if len(sys.argv) > 1:
        timedWeather(sys.argv[1])
    else:
        currentWeather()